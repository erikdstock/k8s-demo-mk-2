#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

cat << EOF > linkerd.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: linkerd
  namespace: argocd
spec:
  project: default
  source:
    chart: linkerd2
    repoURL: https://helm.linkerd.io/stable
    targetRevision: 2.10.1
    helm:
      parameters:
      - name: identityTrustAnchorsPEM
        value: |
          -----BEGIN CERTIFICATE-----
          -----END CERTIFICATE-----
      - name: identity.issuer.scheme
        value: kubernetes.io/tls
      - name: installNamespace
        value: "false"
      - name: cniEnabled
        value: "true"
      valueFiles:
      - values.yaml
      - values-ha.yaml
  destination:
    namespace: linkerd
    server: https://kubernetes.default.svc
  ignoreDifferences:
  - group: ""
    kind: Secret
    name:  linkerd-proxy-injector-tls
    jsonPointers:
    - /data/crt.pem
    - /data/key.pem
  - group: ""
    kind: Secret
    name:  linkerd-sp-validator-tls
    jsonPointers:
    - /data/crt.pem
    - /data/key.pem
  - group: ""
    kind: Secret
    name:  linkerd-tap-tls
    jsonPointers:
    - /data/crt.pem
    - /data/key.pem
  - group: admissionregistration.k8s.io/v1beta1
    kind: MutatingWebhookConfiguration
    name:  linkerd-proxy-injector-webhook-config
    jsonPointers:
    - /webhooks/0/clientConfig/caBundle
  - group: admissionregistration.k8s.io/v1beta1
    kind: ValidatingWebhookConfiguration
    name:  linkerd-sp-validator-webhook-config
    jsonPointers:
    - /webhooks/0/clientConfig/caBundle
  - group: apiregistration.k8s.io/v1
    kind: APIService
    name: v1alpha1.tap.linkerd.io
    jsonPointers:
    - /spec/caBundle
  syncPolicy:
    automated: {}
EOF

mv linkerd.yaml k8s-core

trust_anchor=$(kubectl -n linkerd get secret linkerd-trust-anchor -ojsonpath="{.data['tls\.crt']}" | base64 -d -w 0 -)

echo "There's an less avoidable manual step (maybe someone can figure this out) here due to potential issues line breaks"
echo "Please follow the guide at https://linkerd.io/2.10/tasks/gitops/ and add the following value, with line breaks inserted on your own, to the appropriate portion of k8s-core/linkerd.yaml:"
echo ""
echo ""
echo ""
echo ""
echo $trust_anchor

echo "Please verify that you've correctly added the line breaks here."
echo "When you are done, please commit this, push to master and sync the k8s-core-app project."
echo ""
echo "You can verify that this works by navigating to the linkerd project in your ArgoCD panel and making sure that all pods are in the Ready state."

echo "TODO: High Availability Linkerd Setup"

echo "After you've pushed - you can try manually sync'ing the linkerd project in the ArgoCD console. There may be two stubborn resources that show 'OutOfSync', causing the project itself to appear OutOfSync, but that's okay."
echo "If you're worried - download the linkerd cli tool and run linkerd check - it should come out green"
