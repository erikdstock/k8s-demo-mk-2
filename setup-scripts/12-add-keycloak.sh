#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

PGPASSWORD=$(kubectl get secret keycloak.sso-postgres.credentials.postgresql.acid.zalan.do -o 'jsonpath={.data.password}' -n keycloak| base64 -d)

cat << EOF > sso-db-unsealed-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-db-secret
  namespace: keycloak
stringData:
  POSTGRES_DATABASE: "root"
  POSTGRES_EXTERNAL_ADDRESS: "sso-postgres.keycloak.svc.cluster.local"
  POSTGRES_EXTERNAL_PORT: "5432"
  POSTGRES_HOST: "sso-postgres"
  POSTGRES_PASSWORD: $PGPASSWORD
  POSTGRES_USERNAME: "keycloak"
EOF

echo "Note: The above must be named keycloak-db-secret and the db must be called root."
echo "At writing time, this is hardcoded in the keycloak operator"

kubeseal --format=yaml < sso-db-unsealed-secret.yaml > sealed-sso-db-secret.yaml

cat << EOF > sso.yaml
apiVersion: keycloak.org/v1alpha1
kind: Keycloak
metadata:
  name: sso
  labels:
    keycloak: sso
    app: sso
spec:
  instances: 2
  externalDatabase:
    enabled: true
  keycloakDeploymentSpec:
    experimental:
      env:
        - name: JAVA_OPTS_APPEND
          value: >-
            -XX:+UseContainerSupport
            -XX:MaxRAMPercentage=50.0
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - keycloak
              topologyKey: "topology.kubernetes.io/zone"
          - weight: 90
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - keycloak
              topologyKey: "kubernetes.io/hostname"
EOF

mv sealed-sso-db-secret.yaml auth/sso
mv sso.yaml auth/sso

rm sso-db-unsealed-secret.yaml

git add auth
git commit -m "add db secret and keycloak custom resource"
git push origin master

argocd app sync sso

echo "This will take a little while (maybe 7-8 minutes to get both pods running)"
